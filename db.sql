DROP TABLE IF EXISTS customer;
CREATE TABLE customer (
                          id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
                          first_name VARCHAR (256),
                          last_name VARCHAR (256),
                          mail VARCHAR (256),
                          tel VARCHAR (256),
                          addresses LONGTEXT,
                          city LONGTEXT,
                          country LONGTEXT
);
DROP TABLE IF EXISTS product;
CREATE TABLE product (
                         id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
                         name VARCHAR(256),
                         description_product LONGTEXT,
                         price_product FLOAT,
                         supplier LONGTEXT,
                         web_site_supplier LONGTEXT
);
DROP TABLE IF EXISTS sale;
CREATE TABLE sale (
                      id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
                      date_of_purchase DATETIME,
                      customer_id INT,
                      amount_sales FLOAT,
                      FOREIGN KEY (customer_id) REFERENCES customer (id)
);
DROP TABLE IF EXISTS sale_product;
CREATE TABLE sale_product (
                              id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
                              product_id INT,
                              sale_id INT,
                              quantity_product INT,
                              color VARCHAR(256),
                              FOREIGN KEY (product_id) REFERENCES product (id),
                              FOREIGN KEY (sale_id) REFERENCES sale (id)
);
CREATE TABLE tag (
                     id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
                     name VARCHAR(256),
                     product_id INT,
                     FOREIGN KEY (product_id) REFERENCES product (id)
)