CREATE TABLE customer (
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    first_name TEXT,
    last_name TEXT,
    mail TEXT,
    tel INTEGER,
    addresses TEXT,
    city TEXT,
    country TEXT
);

CREATE TABLE product (
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    name varchar(200),
    description_product TEXT,
    price_product money,
    supplier TEXT,
    web_site_supplier TEXT
);

CREATE TABLE sale (
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    date_of_purchase TEXT NOT NULL,
    customer_id INTEGER,
    amount_sales money,
    FOREIGN KEY (customer_id) REFERENCES customer(id)
);

CREATE TABLE sale_product (
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    product_id TEXT,
    sale_id INTEGER,
    quantity_product INTEGER,
    color TEXT,
    FOREIGN KEY (product_id) REFERENCES product(id),
    FOREIGN KEY (sale_id) REFERENCES sale(id)
);

CREATE TABLE tag (
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    name TEXT,
    product_id INTEGER,
    FOREIGN KEY (product_id) REFERENCES product(id)
)
