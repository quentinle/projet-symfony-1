<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SaleProduct
 *
 * @ORM\Table(name="sale_product", indexes={@ORM\Index(name="product_id", columns={"product_id"}), @ORM\Index(name="sale_id", columns={"sale_id"})})
 * @ORM\Entity
 */
class SaleProduct
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="product_id", type="integer", nullable=true)
     */
    private $productId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="sale_id", type="integer", nullable=true)
     */
    private $saleId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="quantity_product", type="integer", nullable=true)
     */
    private $quantityProduct;

    /**
     * @var string|null
     *
     * @ORM\Column(name="color", type="string", length=256, nullable=true)
     */
    private $color;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProductId(): ?int
    {
        return $this->productId;
    }

    public function setProductId(?int $productId): self
    {
        $this->productId = $productId;

        return $this;
    }

    public function getSaleId(): ?int
    {
        return $this->saleId;
    }

    public function setSaleId(?int $saleId): self
    {
        $this->saleId = $saleId;

        return $this;
    }

    public function getQuantityProduct(): ?int
    {
        return $this->quantityProduct;
    }

    public function setQuantityProduct(?int $quantityProduct): self
    {
        $this->quantityProduct = $quantityProduct;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }


}
