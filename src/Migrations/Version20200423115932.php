<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200423115932 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE customer CHANGE first_name first_name VARCHAR(256) DEFAULT NULL, CHANGE tel tel VARCHAR(256) DEFAULT NULL, CHANGE addresses addresses LONGTEXT DEFAULT NULL, CHANGE city city LONGTEXT DEFAULT NULL, CHANGE country country LONGTEXT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE customer CHANGE first_name first_name VARCHAR(256) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, CHANGE tel tel VARCHAR(256) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, CHANGE addresses addresses LONGTEXT CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, CHANGE city city LONGTEXT CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, CHANGE country country LONGTEXT CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`');
    }
}
